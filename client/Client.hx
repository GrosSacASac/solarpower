package;

using tink.CoreApi;

@:expose("NetBridge") @:keep @:tink
class Client {
	static function main() {
	}

	static var client:Client;
	
	static public function connect(url:String, playerId:String) {
		if (client != null) throw "Only a single instance is allowed";
		client = new Client(url, playerId);
		return client.connectWS();
	}
	
	var service:WebSocketService;
	
	var url:String = _;
	var playerId:String = _;
	var state:GameState = untyped __js__("window.state");
	
	function connectWS() {
		return new js.Promise((resolve, reject) -> {
			trace('Connecting: url=$url playerId=$playerId');
			service = new WebSocketService(url);
			
			service.onConnected.handle(() -> {
				trace('Service connected');
				greet().handle(_ -> {
					listen();
					resolve({
						send: service.send,
						sendInputs: sendInputs,
						handle: service.onMessage.handle,
						close: service.close
					});
				});
			});
			
			service.onDisconnected.handle(result -> {
				trace('Service closed: code=${result.code} reason=${result.reason}');
				reject({
					code: result.code,
					reason: result.reason
				});
			});
			
			service.onError.handle(error -> trace('Service error: $error'));
		});
	}
	
	function greet() {
		return Future.async(function (cb) {
			service.onMessage.filter(obj -> obj.ready).nextTime().handle(_ -> {
				trace("Received ready message");
				state.otherPlayerReady = true;
				cb(Noise);
			});
			service.onMessage.filter(obj -> obj.action == "setState").nextTime().handle(obj -> {
				trace("Received initial state");
				setState(obj.newState);
			});
			trace("Sending greetings");
			service.send({greetings: true, who: playerId});
		});
	}
	
	function listen() {
		trace("Listening for messages");
		service.onMessage.handle(function (obj) {
			if (Reflect.hasField(obj, "ready")) {
				state.otherPlayerReady = obj.ready;
			}
			else if (Reflect.hasField(obj, "action")) {
				switch obj.action {
					case "setState": setState(obj.newState);
					case other: trace('Unrecognized action: $other');
				}
			}
			else {
				trace('Unrecognized message: ${haxe.Json.stringify(obj, null, "  ")}');
			}
		});
	}
	
	function sendInputs() {
		for (input in state.inputs) service.send(input);
		state.inputs = [];
	}
	
	function setState(newState) {
		untyped __js__('Object.assign({0}, {1})', state, newState);
		state.freshState = true;
	}
}

typedef GameState = {
	otherPlayerReady:Bool,
	inputs:Array<Dynamic>,
	freshState:Bool
}
