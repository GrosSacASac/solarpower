package;

import haxe.Json;
import js.html.CloseEvent;
import js.html.Event;
import js.html.MessageEvent;
import js.html.WebSocket;

using tink.CoreApi;

@:tink class WebSocketService {
	@:signal var onConnected:Noise;
	@:signal var onDisconnected:{code:Int, reason:String};
	@:signal var onMessage:Dynamic;
	@:signal var onError:String;
	
	@:forward(url, close)
	var ws:WebSocket;
	
	public function new(url:String) {
		ws = new WebSocket(url);
		ws.onopen = () -> _onConnected.trigger(Noise);
		ws.onclose = (e:CloseEvent) -> _onDisconnected.trigger({code: e.code, reason: e.reason});
		ws.onerror = (e:Event) -> _onError.trigger("WebSocket error");
		ws.onmessage = (e:MessageEvent) -> {
			var obj:Dynamic;
			try {
				obj = Json.parse(e.data);
			}
			catch (err:Dynamic) {
				_onError.trigger("JSON parse error");
				return;
			}
			_onMessage.trigger(obj);
		}
	}
	
	public function send(obj:Dynamic) {
		if (ws.readyState == 1) ws.send(Json.stringify(obj));
		else _onError.trigger('WebSocket is not ready: readyState=${ws.readyState}');
	}
}
