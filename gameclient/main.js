
import {render} from "./render/render.js";

let player1 = {
	"name": "player1",
	"power": 0,
};
let player2 = {
	"name": null,
	"power": 0,
};

window.state = {
    inputs: [],
    otherPlayerReady: false,
    time: 0,
    players: [player1, player2],
    cannons: [],
    bullets: [],
    walls: [],
	freshState: false,
	gameOver: false
};



let netBridge;

const loop = function (time) {
    if (!state.gameOver) {
        requestAnimationFrame(loop);
    }

    state.time = time;
    if (netBridge) {
        netBridge.sendInputs();
    }
	if (window.state.freshState) {
		render(state);
		window.state.freshState = false;
	}

};
loop(0);

NetBridge.connect("ws:///localhost:7999", player).then(function (service) {
    netBridge = service;
});
