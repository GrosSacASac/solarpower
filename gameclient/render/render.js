let hit = $("<div class='flash'></div>");

let money1 = $("<span class='money-1'>Money: 0</span>");
let money2 = $("<span class='money-2'>Money: 0</span>");

let message = $('<div class="message">Player 1 has won!</div>');

let time = $("<span class='time'>5:00</span>");
let sunContainer = $("<div class='sun-container'></div>");
let sun = $("<img class='sun'>");
let ground = $("<div class='ground'></div>");
let box1 = $("<img class='box-1'>");
let box2 = $("<img class='box-2'>");

let cannon = $("<img class='cannon'>");
let bullet = $("<div class='bullet'></div>");

let laser = $("<img class='laser'>");
let ray = $("<img class='ray'>");

let trump = $("<img class='trump'>");
let rocket = $("<img class='rocket'>");

let wall = $("<img class='wall'>");
let mirror = $("<img class='mirror'>");
let antiTrump = $("<img class='anti-trump'>");


sun.attr("src", "../assets/sun.svg");

box1.attr("src", "../assets/boxes.svg");

box2.attr("src", "../assets/boxes.svg");


cannon.attr("src", "../assets/cannon.svg");
cannon.attr("data-type", "cannon");

laser.attr("src", "../assets/laser.svg");
laser.attr("data-type", "laser");

rocket.attr("src", "../assets/trump.svg");

trump.attr("src", "../assets/trump.svg");
trump.attr("data-type", "trump");

wall.attr("src", "../assets/wall.svg");
wall.attr("data-type", "wall");

mirror.attr("src", "../assets/mirror.svg");
mirror.attr("data-type", "mirror");


antiTrump.attr("src", "../assets/antitrump.png");
antiTrump.attr("data-type", "antiTrump");

var canvas = null;

$(function () {
	canvas = $("#game");
	// canvas.append(sun);
	canvas.append(hit);
	$('.flash').hide();
	canvas.append(ground);
	canvas.append(sunContainer);

	if(player === "player1"){
		loadPlayer1Gui();

	}
	else{
		loadPlayer2Gui();

	}
	canvas.append(time);
	// canvas.append(power1);
	// canvas.append(power2);
	canvas.append(message);


	let selectedElement = null;

	//set the selected element
	$("body").on("mousedown", function (ev) {
		const el = $(ev.target);
		let type = el.data("type");

		if (!type) {
			return;
		}
		if ((player === "player1" && !["cannon", "trump", "laser"].includes(type)) ||
		   (player === "player2" && !["wall", "mirror", "antiTrump"].includes(type))) {
		    return;
		}
		selectedElement = el;

		ev.preventDefault();
	})


	// get the position of the mouse on mouse up
	$("body").on("mouseup", function (ev) {
		if (selectedElement === null)
			return;
		ev.preventDefault();

		let type = selectedElement.data("type");
		let index = selectedElement.data("index");

		var x = ev.pageX - $('#game').offset().left;
		var y = ev.pageY - $('#game').offset().top;

		const positionToDrop = x;

		if (!(x > 100 && x < 900)) {
			return;
		}
		if (!index) {
			state.inputs.push({
				"action": "drop",
				"what": type,
				"x": positionToDrop - selectedElement[0].width/2,
			});
		} else {
			state.inputs.push({
				"action": "move",
				"what": type,
				"x": positionToDrop - selectedElement[0].width/2,
				index
			});
		}


		selectedElement = null;
	});
});

const toClear = [];
function loadPlayer2Gui() {
	canvas.append(box2);
	canvas.append(wall);
	canvas.append(mirror);
	canvas.append(antiTrump);
	canvas.append(money2);
}

function loadPlayer1Gui() {
	canvas.append(box1);
	canvas.append(cannon);
	canvas.append(laser);
	canvas.append(trump);
	canvas.append(money1);
}

const cannonsClones = [];
const bulletClones = [];

const laserClones = [];
const rayClones = [];

const trumpClones = [];
const rocketClones = [];

const wallClones = [];
const mirrorClones = [];
const antiTrumpClones = [];
const sunClones = [];

const cloneWithIndex = (original, i) => {
	const clone = original.clone();
	clone.data("index", String(i));
	canvas.append(clone);
	return clone;
};

function formatTime(time) {
    var minutes = Math.floor(time / 60);
    var seconds = Math.floor(time - 60 * minutes);
    return minutes.toString() + ":" + ((seconds < 10) ? "0" : "") + seconds;
}

let lastHitPoints = 0;

function render(state) {
    if (state.hitPoints < lastHitPoints) {
      $('.flash')
          .show()
          .animate({opacity: 0.5}, 300) 
          .fadeOut(300)
          .css({'opacity': 1});
    }
    lastHitPoints = state.hitPoints;

	//render hitpoints
	for (let i = 0; i < state.hitPoints; i++) {
		if (!sunClones[i]) {
			let clone = sun.clone();
			sunContainer.append(clone);
			sunClones.push(clone);
		}
	}

	for(let i = state.hitPoints; i <= sunClones.length; i++ ){
		if (sunClones[i]) {sunClones[i].remove();}
	}
	sunClones.length = state.hitPoints;
	//render timer
	time.text(formatTime(state.timeLeft));

	//render money
	money1.text("Money: " + state.money1);
	money2.text("Money: " + state.money2);

	state.cannons.forEach(function (el, i) {
		let x = el.x;
		let clonedCannon = cannonsClones[i];

		if (!clonedCannon) {
			clonedCannon = cloneWithIndex(cannon, i);
			clonedCannon.css({
				"bottom": "91px"
			});
			cannonsClones.push(clonedCannon);
		}

		clonedCannon.css({
			"left": x,
		});
	});

	// remove extra
	cannonsClones.slice(state.cannons.length).forEach(function (el) {
		el.remove();
	});
	cannonsClones.length = state.cannons.length;

	state.bullets.forEach(function (el, i) {
		let x = el.x;
		let y = el.y;
		let clonedBullet = bulletClones[i];

		if (!clonedBullet) {
			clonedBullet = cloneWithIndex(bullet, i);
			clonedBullet.css({
				"background-color": getRandomColor()
			});
			bulletClones.push(clonedBullet);
		}

		clonedBullet.css({
			"top": y,
			// "left": x + cannon[0].width/2 + "px",
			"left": x,
		});
	});

	// remove extra
	bulletClones.slice(state.bullets.length).forEach(function (el) {
		el.remove();
	});
	bulletClones.length = state.bullets.length;

	state.lasers.forEach(function (el, i) {
		let x = el.x;
		let y = el.y;
		let clonedLaser = laserClones[i];

		if (!clonedLaser) {
			clonedLaser = cloneWithIndex(laser, i);
			laserClones.push(clonedLaser);
		}

		clonedLaser.css({
			"bottom": "98px",
			"left": x
		});
	});

	// remove extra
	laserClones.slice(state.lasers.length).forEach(function (el) {
		el.remove();
	});
	laserClones.length = state.lasers.length;

	state.rays.forEach(function (el, i) {
		let x = el.x;
		let y = el.y;
		let clonedRay = rayClones[i];

		if (!clonedRay) {
			clonedRay = cloneWithIndex(ray, i);
			rayClones.push(clonedRay);
		}
		clonedRay.css({
			"top": y,
			// "left": x + laser[0].width/2 + "px",
			"left": x,
		});
	});

	// remove extra
	rayClones.slice(state.rays.length).forEach(function (el) {
		el.remove();
	});
	rayClones.length = state.rays.length;

	state.trumps.forEach(function (el, i) {
		let x = el.x;
		let clonedTrump = trumpClones[i];

		if (!clonedTrump) {
			clonedTrump = cloneWithIndex(trump, i);
			clonedTrump.css({
				"bottom": "91px"
			});
			trumpClones.push(clonedTrump);
		}

		clonedTrump.css({
			"left": x,
		});
	});

	// remove extra
	trumpClones.slice(state.trumps.length).forEach(function (el) {
		el.remove();
	});
	trumpClones.length = state.trumps.length;

	state.rockets.forEach(function (el, i) {
		let x = el.x;
		let y = el.y;
		let clonedRocket = rocketClones[i];

		if (!clonedRocket) {
			clonedRocket = cloneWithIndex(rocket, i);
			clonedRocket.addClass("rotate");
			rocketClones.push(clonedRocket);
		}

		clonedRocket.css({
			"top": y,
			"left": x
		});
	});

	// remove extra
	rocketClones.slice(state.rockets.length).forEach(function (el) {
		el.remove();
	});
	rocketClones.length = state.rockets.length;

	state.walls.forEach(function (el, i) {
		let x = el.x;
		let clonedWall = wallClones[i];

		if (!clonedWall) {
			clonedWall = cloneWithIndex(wall, i);
			clonedWall.css({
				"top": "200px",
				"transform": "translateY(25px)",
			});
			wallClones.push(clonedWall);
		}

		clonedWall.css({
			"left": x,
		});
	});

	// remove extra
	wallClones.slice(state.walls.length).forEach(function (el) {
		el.remove();
	});

	wallClones.length = state.walls.length;

	state.mirrors.forEach(function (el, i) {
		let x = el.x;
		let y = el.y;
		let clonedMirror = mirrorClones[i];

		if (!clonedMirror) {
			clonedMirror = cloneWithIndex(mirror, i);
			clonedMirror.css({
				"transform": "translateY(-13px)",
			});
			mirrorClones.push(clonedMirror);
		}

		clonedMirror.css({
			"top": "200px",
			"left": x
		});
	});

	// remove extra
	mirrorClones.slice(state.mirrors.length).forEach(function (el) {
		el.remove();
	});
	mirrorClones.length = state.mirrors.length;


	state.antiTrumps.forEach(function (el, i) {
		let x = el.x;
		let clonedAntiTrump = antiTrumpClones[i];

		if (!clonedAntiTrump) {

			clonedAntiTrump = cloneWithIndex(antiTrump, i);
			clonedAntiTrump.css({
				"top": "200px"
			});
			antiTrumpClones.push(clonedAntiTrump);
		}

		clonedAntiTrump.css({
			"left": x,
		});
	});

	// remove extra
	antiTrumpClones.slice(state.antiTrumps.length).forEach(function (el) {
		el.remove();
	});
	antiTrumpClones.length = state.antiTrumps.length;
	
	if(state.gameOver) {
		let winner = state.winner;
		if (winner === "player1") {
			snow.start();
		}
		message.css("display", "block");
		message.text(winner + " has won the game, congratz!");
	}
}

//util functions
function getRandomColor() {
	var letters = '0123456789ABCDEF';
	var color = '#';
	for (var i = 0; i < 6; i++) {
		color += letters[Math.floor(Math.random() * 16)];
	}
	return color;
}


export { render };
