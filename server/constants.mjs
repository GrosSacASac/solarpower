export {
    roundTime,
    attackY,
    defenseY,
    defenseItemWidth,
    sunHitPoints,
    startingMoney,
    incrementMoneyInterval,
    itemPrices,
    cannonFirstShotDelay,
    cannonCooldownTime,
    cannonBulletSpeed,
    cannonBulletDamage,
    laserFirstShotDelay,
    laserCooldownTime,
    laserRaySpeed,
    laserRayDamage,
    trumpFirstShotDelay,
    trumpCooldownTime,
    trumpRocketSpeed,
    trumpRocketDamage,
    updateFrequency
};

const roundTime = 120; // Game duration

const attackY = 800; // Y position of the attack (player 1) line
const defenseY = 230; // Y position of the defense (player 2) line
const defenseItemWidth = 60; // Width of defense (player 2) items

const sunHitPoints = 5; // Initial value for sun's hit points

const startingMoney = 5; // Initial value for both players' money.
const incrementMoneyInterval = 5; // Interval in seconds when the money is incremented.
const itemPrices = { // Item prices
    cannon: 1,
    laser: 2,
    trump: 4,
    wall: 1,
    mirror: 2,
    antiTrump: 4
};

const cannonFirstShotDelay = 1000; // Delay before a newly placed cannon fires it's first shot
const cannonCooldownTime = 5000; // Cannon firing interval
const cannonBulletSpeed = 4; // Cannon bullet speed, pixels per game tick
const cannonBulletDamage = 1; // Cannon bullet damage

const laserFirstShotDelay = 2000; // Delay before a newly placed laser fires it's first shot
const laserCooldownTime = 10000; // Laser firing interval
const laserRaySpeed = 12; // Laser ray speed, pixels per game tick
const laserRayDamage = 2; // Laser ray damage

const trumpFirstShotDelay = 3000; // Delay before a newly placed trump fires it's first shot
const trumpCooldownTime = 15000; // Trump firing interval
const trumpRocketSpeed = 8; // Trump rocket speed, pixels per game tick
const trumpRocketDamage = 4; // Trump rocket damage

const updateFrequency = 50; // Game loop update frequency in milliseconds
