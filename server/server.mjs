import WebSocket from 'ws';

import updateTimer from './updateTimer';
import updateMoney from './updateMoney';
import updateCannons from './updateCannons';
import updateBullets from './updateBullets';

import {
    roundTime,
    attackY,
    defenseY,
    sunHitPoints,
    startingMoney,
    itemPrices,
    cannonFirstShotDelay,
    cannonCooldownTime,
    cannonBulletSpeed,
    cannonBulletDamage,
    laserFirstShotDelay,
    laserCooldownTime,
    laserRaySpeed,
    laserRayDamage,
    trumpFirstShotDelay,
    trumpCooldownTime,
    trumpRocketSpeed,
    trumpRocketDamage,
    updateFrequency
} from './constants';

const WS_PORT = 7999;

let player1;
let player2;
let ready = false;

let gameState = {
    hitPoints: sunHitPoints,
    money1: startingMoney,
    money2: startingMoney,
    cannons: [],
    bullets: [],
    lasers: [],
    rays: [],
    trumps: [],
    rockets: [],
    walls: [],
    mirrors: [],
    antiTrumps: [],
    timeLeft: roundTime,
    gameOver: false,
    winner: undefined
};

let privateGameState = {
    time: 0,
    lastIncrementMoneyTime: 0
};

const updateGame = () => {
    if (!ready || gameState.gameOver) return;

    privateGameState.time += updateFrequency;

    updateTimer(gameState, privateGameState);
    updateMoney(gameState, privateGameState);
    
    updateCannons(gameState, privateGameState, "cannons", "bullets", cannonFirstShotDelay, cannonCooldownTime);
    updateBullets(gameState, privateGameState, "bullets", "walls", cannonBulletSpeed, cannonBulletDamage);
    
    updateCannons(gameState, privateGameState, "lasers", "rays", laserFirstShotDelay, laserCooldownTime);
    updateBullets(gameState, privateGameState, "rays", "mirrors", laserRaySpeed, laserRayDamage);

    updateCannons(gameState, privateGameState, "trumps", "rockets", trumpFirstShotDelay, trumpCooldownTime);
    updateBullets(gameState, privateGameState, "rockets", "antiTrumps", trumpRocketSpeed, trumpRocketDamage);

    if (gameState.hitPoints <= 0) {
        gameState.gameOver = true;
        gameState.winner = "player1";
    }

    //console.log("state was updated and sent");
    //console.log(gameState);
    sendStateToPlayers();
};

const sendToPlayer = (player, thing) => {
    if (player) {
        if (player.readyState === 1) player.send(JSON.stringify(thing));
        else console.warn(`WebSocket is not ready: readyState=${player.readyState}`);
    }
};

const sendStateToPlayers = (player, thing) => {
    [player1, player2].forEach(player => {
        sendToPlayer(player, {action: "setState", newState: gameState});
    });
};

const handleGreet = (ws, details) => {
    if (details.who === "player1") {
        player1 = ws;
    } else {
        player2 = ws;
    }
    sendStateToPlayers();
    if (player1 && player2) {
        ready = true;
        sendToPlayer(player1, {ready});
        sendToPlayer(player2, {ready});
    }
};

const relayMessage = (ws, details) => {
    let target;
    if (ws === player1) {
        target = player2;
    } else {
        target = player1;
    }
    if (!target) {
        console.warn("other player has not yet greeted");
        return;
    }
    sendToPlayer(target, details);
};

const handleMessage = (ws, details) => details.action ? handleAction(ws, details) : false;

const buyItem = (what, moneyProperty) => {
    const price = itemPrices[what];
    if (!price) console.warn(`Missing item price: what=${what}`);
    if (gameState[moneyProperty] >= price) {
        gameState[moneyProperty] -= price;
        console.log(`${moneyProperty}: Bought ${what} for ${price}, money left: ${gameState[moneyProperty]}`);
        return true;
    }
    else {
        return false;
    }
};

const dropWeapon = (itemProperty, action) => {
    if (buyItem(action.what, "money1")) {
        gameState[itemProperty].push({
            x: action.x,
            angle: 0,
            lastTimeShot: 0
        })
    }
};

const dropShield = (itemProperty, action) => {
    if (buyItem(action.what, "money2")) {
        gameState[itemProperty].push({
            x: action.x
        });
    }
};

const moveItem = (itemProperty, action) => {
    if (action.index >= 0 && action.index < gameState[itemProperty].length) {
        gameState[itemProperty][action.index].x = action.x;
    } else {
        console.warn(`move: ${itemProperty}: Array index ${action.index} is out of bounds`);
    }
};

const itemActionHandlers = {
    cannon: {property: "cannons", drop: dropWeapon, move: moveItem},
    laser: {property: "lasers", drop: dropWeapon, move: moveItem},
    trump: {property: "trumps", drop: dropWeapon, move: moveItem},
    wall: {property: "walls", drop: dropShield, move: moveItem},
    mirror: {property: "mirrors", drop: dropShield, move: moveItem},
    antiTrump: {property: "antiTrumps", drop: dropShield, move: moveItem}
};

const handleAction = (ws, action) => {
    if (action.what) {
        const h = itemActionHandlers[action.what];
        if (h && h[action.action]) {
            h[action.action](h.property, action);
            return true;
        }
    }
    return false;
};

const handleLeave = (ws) => {
    ready = false;
    if (ws === player1) {
        player1 = undefined;
    }
    if (ws === player2) {
        player2 = undefined;
    }
    sendToPlayer(player1, {ready});
    sendToPlayer(player2, {ready});
};

const wss = new WebSocket.Server({
    port: WS_PORT,
});

wss.on('connection', function connection(ws) {
    ws.on('message', function incoming(message) {
        console.log('received', message);
        const parsed = JSON.parse(message);
        if (parsed.greetings) {
            handleGreet(ws, parsed);
        } else if (parsed.relayed) {
            relayMessage(ws, parsed);
        } else if (!handleMessage(ws, parsed)) {
            console.warn(`unexpected message: "${message}"`);
        }
    });

    ws.on('close', function (code, reason) {
        console.log('closed, reason:', reason || code.reason);
        handleLeave(ws);
    });

});

console.log("listening on port", WS_PORT);
setInterval(updateGame, updateFrequency);
