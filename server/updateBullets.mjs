import {
    defenseY,
    defenseItemWidth
} from './constants';

export default function(gameState, privateGameState, bulletsProperty, wallsProperty, speed, damage) {
    const maxDistance = defenseItemWidth / 2;
    const toKeep = [];
    let isModified = false;
    gameState[bulletsProperty].forEach(bullet => {
        bullet.y -= speed;
        if (bullet.y > defenseY || (bullet.y > 0 && bullet.willHit)) {
            toKeep.push(bullet);
        } else if (bullet.y > 0) {
            if (gameState[wallsProperty].some(wall => Math.abs(wall.x - bullet.x) <= maxDistance)) {
                console.log(`intercepted type=${bulletsProperty}`);
                isModified = true;
            }
            else {
              bullet.willHit = true;
              toKeep.push(bullet);
            }
        } else {
            gameState.hitPoints = Math.max(gameState.hitPoints - damage, 0);
            console.log(`hit type=${bulletsProperty} damage=${damage} hp=${gameState.hitPoints}`);
            isModified = true;
        }
    });
    //if (gameState[bulletsProperty].length > 0) console.log(gameState[bulletsProperty][0]);
    if (isModified) gameState[bulletsProperty] = toKeep;
};
