import {
    attackY
} from './constants';

export default function(gameState, privateGameState, cannonsProperty, bulletsProperty, firstShotDelay, cooldownTime) {
    gameState[cannonsProperty].forEach(cannon => {
        if (cannon.lastTimeShot == 0) {
            // Cannon was just placed, set `lastTimeShot` to a fake time so that the first shot will happen `firstShotDelay` later.
            cannon.lastTimeShot = privateGameState.time - cooldownTime + firstShotDelay;
        }
        if (privateGameState.time < cannon.lastTimeShot + cooldownTime) {
            return;
        }
        cannon.lastTimeShot = privateGameState.time;
        gameState[bulletsProperty].push({x: cannon.x, y: attackY});
    });
};
