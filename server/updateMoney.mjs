import {
    incrementMoneyInterval
} from './constants';

export default function(gameState, privateGameState) {
    if (privateGameState.time < privateGameState.lastIncrementMoneyTime + 1000 * incrementMoneyInterval) {
        return;
    }
    privateGameState.lastIncrementMoneyTime = privateGameState.time;
    ++gameState.money1;
    ++gameState.money2;
    console.log(`money1=${gameState.money1} money2=${gameState.money2}`);
};
