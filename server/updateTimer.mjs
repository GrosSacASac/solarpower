let lastTime = 0;

export default function(gameState, privateGameState) {
    if (lastTime != 0) {
        gameState.timeLeft -= (privateGameState.time - lastTime) / 1000;
    }
    lastTime = privateGameState.time;
    if (gameState.timeLeft <= 0) {
        gameState.timeLeft = 0;
        gameState.gameOver = true;
        gameState.winner = "player2";
    }
};
